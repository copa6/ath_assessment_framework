import pandas as pd
from Helper import CompareCodesHelper



r_function_weights_csv = "rfunction_weights.csv"
reference_codes_file = "reference_code_1.R"
reference_output_csv = "reference_output.csv"
user_jupyter_notebook = "user_02.ipynb"


comparison_helper = CompareCodesHelper(r_function_weights_csv)
user_calls = comparison_helper.get_function_calls(user_jupyter_notebook, file_type="user")
ref_calls = comparison_helper.get_function_calls(reference_codes_file, file_type="ref")
percent_code_match, match, unmatch_user, unmatch_reference = comparison_helper.compare_codes(ref_calls, user_calls)

user_output_data = comparison_helper.read_data_from_json_data(user_jupyter_notebook, get_output=True)
ref_output_data = comparison_helper.read_from_csv(reference_output_csv, file_type="ref", sep='|')
percent_output_match = comparison_helper.compare_r_outputs(ref_output_data, user_output_data)

print('\n')
print('output match: {0:.2f}% '.format(percent_output_match))
print('\n================================================================\n')
print('code match: {0:.2f}% '.format(percent_code_match))
print('\n================================================================\n')
print(str('Matched Functions: ' + str(match)))
print('\n================================================================\n')
print('Unmatched Reference Functions: ' + str(unmatch_reference))
print('\n================================================================\n')
print('Unmatched User Functions: ' + str(unmatch_user))
print('\n================================================================\n')


