import json
import re, os
import pprint
import pandas as pd
from Helper import CompareCodesHelper


helper_test = CompareCodesHelper("rfunction_weights.csv")
# user_code = helper_test.read_data_from_json_data("ReferenceSteps.ipynb", get_code=True)
# print(user_code)
# user_func = helper_test.get_function_calls("ReferenceSteps.ipynb", file_type="user")
# print(user_func)

# test = helper_test.read_data_from_file("rfunction_weights.csv", file_type="other")
# print(test)

count = 0
test_output_data = helper_test.read_data_from_json_data("user_02.ipynb", get_output=True)
test_ref_data = helper_test.read_from_csv("reference_output.csv", file_type="ref", sep=',')
# print(helper_test.compare_r_outputs(test_ref_data, test_output_data))
print(test_output_data)
# test_df = pd.DataFrame(list(test_output_data.items()), columns=["Parent_Code", "Output"])
# print(test_output_data)

# find_in_list_old = ['X2ndFlrSF	1.487238	1', '1      0.01804421', '524  -10.832815', 'Chisquare = ', 'p = ']
# find_in_list = [re.sub('\s+','\t',find_in) for find_in in find_in_list_old]
# for i,key in enumerate(test_output_data["Key"]):
# 	text_all = test_output_data["Output"][i]
# 	for text in text_all.split("\n"):
# 		text = re.sub('\s+','\t',text)
# 		for find_in in find_in_list:
# 			count += 1
# 			if find_in in text:
# 				# print() 
# 				# print("found")
# 				print(re.findall(r"[-+]?\d*\.\d+\w*[-+]?\d+|[-+]?\d*\.\d+|\d+", text.split(find_in)[1], flags=re.UNICODE)[0])
# 			# print(re.findall(r"[-+]?\d*\.\d+\w*[-+]?\d+", text.split("969\t-4.389047")[1], flags=re.UNICODE))
# print(count)
# print(float('2.60e-05')==0.000026)

# user_calls = helper_test.get_function_calls("ReferenceSteps.ipynb", file_type="user")
# ref_calls = helper_test.get_function_calls("reference_code_1.R", file_type="ref")
# percent_match, match, unmatch_user, unmatch_reference = helper_test.compare_codes(ref_calls, user_calls)
# print(percent_match)
# print(match)
# print(unmatch_reference)
# print(unmatch_user)

for file in helper_test.get_files_list_from_directory(file_type="user"):
	print(os.path.basename(file))


























# with open("E:/Saurabh/ATH/Python_Integration/CompareStepsCode/Codes/ref_out.json", 'r') as ref_json:
# 	t = json.loads(ref_json.read())
# # print(json.loads(t))
# print(t["Out"])
# # print(round(17.74,2))

# from difflib import SequenceMatcher
# print(SequenceMatcher(None, 'RMSE',"RMSE").ratio())
# print(type("asfd/asdf/adsf")==list)
# print(re.findall(r'<-([^(]*)\(', "fasdgfsdgsfd(Sdfsdf"))

# with open("ReferenceSteps(1).ipynb", 'r',  encoding='utf8') as f:
# 	user = f.read()

# pp = pprint.PrettyPrinter()
# user_json = json.loads(user)
# # pp.pprint(user_json["cells"])

# codes = []
# outputs=[]
# print(user)

# for cell in user_json["cells"]:
# 	if cell["cell_type"]=='code':
# 		for source in (cell["source"]):
# 			codes.append(source)



# test = pd.read_csv("E:\\Saurabh\\ATH\\Python_Integration\\CompareStepsCode\\Codes\\Reference\\reference_output.csv", sep="\t")
# for i in range(len(test["Question"])):
# 	q = test["Question"][i]
# 	ref_ans = test["Answer"][i]
# 	err = float(test["Range"][i])
# 	w = float(test["Weight"][i])
# 	sig = int(test["Significant"][i])
# 	typ = test["Type"][i]
# 	print(str(q) + '--' + str(ref_ans) + '--' + str(err) + '--' + str(w) + '--' + str(sig) + '--' + str(typ))
# print(os.path.dirname(os.path.dirname(os.path.realpath(__file__)).replace('\\','/')))

# print("Create new instance of CompareCodesHelper!")
# home = os.path.dirname(os.path.dirname(os.path.realpath(__file__)).replace('\\','/'))
# users_dir = home + '/user_data'
# reference_dir = home + '/reference_data'
# print(users_dir)
# print(os.path.exists(users_dir))
# if not os.path.exists(users_dir):
# 	print("make users dir")
# 	os.mkdir(users_dir)
# if not os.path.exists(reference_dir):
# 	os.mkdir(reference_dir)

