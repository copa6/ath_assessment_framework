import os
import json
import re
import pandas as pd
from bs4 import BeautifulSoup
from collections import OrderedDict


class CompareCodesHelper():
	"""
	Helper Class to hold all the functiions for ATH Assessment Framework
	"""
	
	def __init__(self, r_function_weights_file, py_function_weights_file=None):
		"""
		Initialise the helper and path to base directory, users directory, and reference directory.
		Also initialise the output finder flags
		"""
		# print("Create new instance of CompareCodesHelper!")
		self.home = os.path.dirname(os.path.dirname(os.path.realpath(__file__)).replace('\\','/'))
		self.users_dir = self.initialise_path_and_create_directory('/user_data')
		self.reference_dir = self.initialise_path_and_create_directory('/reference_data')
		self.others_dir = self.initialise_path_and_create_directory('/others')
		self.outputs_dir = self.initialise_path_and_create_directory('/comparison_results')
		self.output_start_flag = "# Paste and run the code for evaluation, as directed by course details"
		self.output_end_flag = "# Paste all your Evaluation codes above this line"
		self.default_encoding = "utf8"

		self.r_function_weights = self.get_function_weights(self.others_dir + '/' +r_function_weights_file)


	def initialise_path_and_create_directory(self, directory_name):
		dir_path = os.path.normpath(self.home + directory_name)
		if not os.path.exists(dir_path):
			os.mkdir(dir_path)

		return dir_path


	def create_absolute_filepath(self, filename, file_type=None):
		return self.reference_dir + '/' + filename if file_type=="ref" else ( 
			self.users_dir + '/' + filename if file_type=="user" else
			self.others_dir + '/' + filename  if file_type=="others" else
			self.outputs_dir + '/' + filename if file_type=="output" else filename)


	def read_from_csv(self, filename, file_type=None, sep='\t'):
		filename = self.create_absolute_filepath(filename, file_type)
		df = pd.read_csv(filename, sep=sep)
		return(df)

	
	def read_data_from_file(self, filename, file_type=None):
		"""
		Reads data from file and returns in string format
		Args:
			filename: Absolute filename
			file_type: either "ref" or "user". Used to decide which directory the file should be read from 
		"""
		data=''
		filename =  self.create_absolute_filepath(filename, file_type)
		if type(filename)==list:
			for code_file in filename:
				with open (code_file, 'r', encoding=self.default_encoding) as f:
					file_data = f.read()
					data += str(file_data)
		else:
			with open (filename, 'r', encoding=self.default_encoding) as f:
				file_data = f.read()
				data += str(file_data)

		return data

	
	def read_data_from_json_data(self, filename, get_code=False, get_output=False):
		"""
		Reads user code and output data from json..
		Based on the flag, either return the user code data or the user output data
		"""
		user_data = json.loads(self.read_data_from_file(filename, file_type="user"))["cells"]
		user_codes = ''
		user_output_key=[]
		user_output = []
		reached_output_break = False
		finished_output_flag = False

		for cell_data in user_data:
			if not reached_output_break:
				if cell_data["cell_type"]=="code":
					for code_data in cell_data["source"]:
						user_codes += code_data + '\n'
				elif cell_data["cell_type"]=="markdown":
					for markdown_data in cell_data["source"]:
						if markdown_data == self.output_start_flag:
							reached_output_break = True

			elif (not finished_output_flag) and get_output:
				code_key =''
				code_output =''
				if cell_data["cell_type"]=="code":
					for code_data in cell_data["source"]:
						code_key += code_data
					for outputs in cell_data["outputs"]:
						try:
							output_html = outputs["data"]["text/html"]
							output_soup = BeautifulSoup(output_html, 'html.parser')
							output_data = output_soup.get_text()

						except (KeyError, TypeError) :
							output_data = outputs["data"]["text/plain"]

						for individual_output in output_data:
							code_output += individual_output
					user_output_key.append(code_key)
					user_output.append(code_output)
				if cell_data["cell_type"]=="markdown":
					for markdown_data in cell_data["source"]:
						if markdown_data == self.output_end_flag:
							finished_output_flag = True
			user_output_df = pd.DataFrame(OrderedDict([
						("Key", user_output_key),
						("Output", user_output)
					]))
		return_data = user_codes if get_code else user_output_df
		return(return_data)


	def get_r_function_calls(self, data):
		r_function_calls = []
		for i, line in enumerate(data.split("\n")):

			#strip all tabs and other whitespaces from the ends of line
			line = line.strip(' \t\n\r')

			#Skip comments
			if line.startswith('#'):
				continue	
			
			try:
				func_call = re.findall(r'<-([^(]*)\(', line)
				if len(func_call) == 0:
					func_call = re.findall(r'<- ([^(]*)\(', line)
				if len(func_call) == 0 and "(" in line:
					func_call = line.split("(")
				
				func_call = func_call[0].strip()

				# set the flag to check if there's any number or special character - [ or ! in the function call
				special_char_flag = bool(re.search(r'[\d\[!{}$.=]', func_call))
				space_flag = bool(func_call.isspace())

				if not special_char_flag and not func_call=='':
					r_function_calls.append(func_call)
					# print(str(i) + ': ' + str(func_call))
			
			except IndexError as e:
				pass
				# print(str(i) + ': ------')
			
		return list(set(r_function_calls))


	def get_py_function_calls(self, data):
		#Add python function call parsing logic here
		py_function_calls = []
		return py_function_calls


	def get_function_calls(self, filename, file_type=None, code="R"):
		"""
		Returns all the R function calls from the given file
		"""
		if file_type=="ref":
			data = self.read_data_from_file(filename, file_type)
		elif file_type=="user":
			data = self.read_data_from_json_data(filename, get_code=True)

		if code=="R":
			function_calls = self.get_r_function_calls(data)
		elif code=="Py":
			function_calls = self.get_py_function_calls(data)
		
		return(function_calls)


	def get_function_weights(self, function_weights_file, sep=','):
		"""
		Read the function weights csv and returns as a dictionary
		"""
		weights_df = pd.read_csv(function_weights_file, sep)
		func_names = weights_df["Function"]
		weights = weights_df["Weight"]
		function_weights_dict = {}
		for func_name,weight in zip(func_names, weights):
			function_weights_dict[func_name] = weight

		return function_weights_dict


	def compare_codes(self, reference_function_calls, user_function_calls, code="R"):
		"""
		Given the reference function calls and the user function calls,
		compare the two and give the output score, matched functions, and unmatched functions
		"""
		match_total = 0
		reference_total = 0
		match = []
		unmatch_reference = []
		unmatch_user = []

		if code=="R":
			for call in reference_function_calls:
				reference_total += self.r_function_weights[call] if call in self.r_function_weights else 0.5
				if call in user_function_calls:
					match.append(call)
					match_total += self.r_function_weights[call] if call in self.r_function_weights else 0.5
		
		elif code=="Py":
			# Put in the python code comparison logic here
			pass


		unmatch_user = [call for call in user_function_calls if call not in match]
		unmatch_reference = [call for call in reference_function_calls if call not in match]


		percent_match = 100 * match_total/reference_total

		return percent_match, match, unmatch_user, unmatch_reference


	def compare_r_outputs(self, reference_output_df, user_output_df):
		if(len(reference_output_df["Answer"]) == 0):
			return -1
		else:
			user_output_score = 0
			ref_output_score = 0
			for i in range(len(reference_output_df["Answer"])):
				ref_key = reference_output_df["Key"][i]
				ref_out_signifier = reference_output_df["Output_Signifier"][i]
				ref_answer = reference_output_df["Answer"][i]
				idx =int(reference_output_df["Position"][i])
				thresh = float(reference_output_df["Threshold"][i])
				weight = float(reference_output_df["Weight"][i])
				significant_digits = int(reference_output_df["Significant_Digits"][i])
				output_type = reference_output_df["Type"][i]
				ref_output_score += weight
				if output_type=="Number":
					ref_answer = float(ref_answer)
					ref_output_range = abs(float(thresh*ref_answer/100))
					ref_answer_low = ref_answer - ref_output_range
					ref_answer_high = ref_answer + ref_output_range
					for j in range(len(user_output_df["Output"])):
						user_key = user_output_df["Key"][j]
						user_output = user_output_df["Output"][j]

						if ref_key in user_key:
							for user_out_line in user_output.split('\n'):
								user_out_line = re.sub(r'\s+','\t', user_out_line)
								ref_out_signifier = re.sub(r'\s+','\t', ref_out_signifier)
								if ref_out_signifier in  user_out_line:
									try:
										user_answer = float(re.findall(r"[-+]?\d*\.\d+\w*[-+]?\d+|[-+]?\d*\.\d+|\d+", user_out_line.split(ref_out_signifier)[1], flags=re.UNICODE)[idx-1])
										if user_answer <= ref_answer_high and user_answer >= ref_answer_low:
											user_output_score += weight
											break
									except IndexError as e:
										pass

			output_match_percent = 100 * user_output_score/ref_output_score
			return output_match_percent


	def write_output_to_file(self, output_filename, percent_output_match, percent_code_match, match, unmatch_reference, unmatch_user):
		output_file = self.create_absolute_filepath(output_filename, file_type="output")
		with open(output_file, 'w') as f:
			f.write('\n')
			f.write('output match: {0:.2f}% '.format(percent_output_match))
			f.write('\n================================================================\n')
			f.write('code match: {0:.2f}% '.format(percent_code_match))
			f.write('\n================================================================\n')
			f.write(str('Matched Functions: ' + str(match)))
			f.write('\n================================================================\n')
			f.write('Unmatched Reference Functions: ' + str(unmatch_reference))
			f.write('\n================================================================\n')
			f.write('Unmatched User Functions: ' + str(unmatch_user))
			f.write('\n================================================================\n')

	
	def get_files_list_from_directory(self, file_type=None, n=1, new_dir=None):
		"""
		Return a list of names of all directories and contained directories/files in a given file path
		Parameters:
			path: string. Path to the directory where the search is to be performed
			n: int. Number of tab spaces to be put before printing. To give a structured look to the output
			new_dir: string. Used in recursive calls to remove the new directory name found in the folders 
		"""

		path = self.users_dir if file_type=="user" else self.reference_dir
		dir_list = []
		for entry in os.scandir(path):
			# if a new directory is found, call recursively to keep going deeper into subfolders until there are no directories, such that all files are printed
			# else print file naem
			if entry.is_dir():
				new_dir = '/' + entry.name
				path += new_dir
				# print('\t'*(n-1) + path)
				n +=1 
				get_dir_list(path, n, new_dir)
				path = ''.join(path.rsplit(new_dir, 1)) #remove the last occurance of the new directory, to go one directory up and continue search
			else:
				# print('\t'*n + str(entry.name))
				dir_name = path +('/') +  str(entry.name)
				dir_list.append(dir_name)
		
		return(dir_list)




