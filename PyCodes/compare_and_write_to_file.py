import os
import pandas as pd
from Helper import CompareCodesHelper



r_function_weights_csv = "rfunction_weights.csv"
reference_codes_file = "reference_code_1.R"
reference_output_csv = "reference_output.csv"
# user_jupyter_notebook = "ReferenceSteps.ipynb"


comparison_helper = CompareCodesHelper(r_function_weights_csv)
ref_calls = comparison_helper.get_function_calls(reference_codes_file, file_type="ref")
ref_output_data = comparison_helper.read_from_csv(reference_output_csv, file_type="ref", sep='|')


for user_file in comparison_helper.get_files_list_from_directory(file_type="user"):
	user_jupyter_notebook = os.path.basename(user_file)
	user_calls = comparison_helper.get_function_calls(user_jupyter_notebook, file_type="user")
	percent_code_match, match, unmatch_user, unmatch_reference = comparison_helper.compare_codes(ref_calls, user_calls)
	user_output_data = comparison_helper.read_data_from_json_data(user_jupyter_notebook, get_output=True)
	percent_output_match = comparison_helper.compare_r_outputs(ref_output_data, user_output_data)

	output_file_name = user_jupyter_notebook.split('.')[0] + '.txt'
	comparison_helper.write_output_to_file(output_file_name, 
											percent_output_match, 
											percent_code_match,
											match,
											unmatch_reference,
											unmatch_user)

	


